FROM selenium/node-chrome-debug:3.4

MAINTAINER angrytester <thx_phila@yahoo.com>

# 切换root用户安装文件
USER root

VOLUME /etc/shm

# 安装字体
RUN apt-get update \
    && apt-get -y install ttf-wqy-microhei ttf-wqy-zenhei \
    && apt-get clean
    
# 设置环境变量    
ENV LANG zh_CN.UTF-8
ENV LANGUAGE zh_CN.UTF-8
ENV LC_ALL zh_CN.UTF-8

# 设置语言环境为中文
RUN locale-gen zh_CN.UTF-8

# 时区设置
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# 切换回seluser用户，否则vnc启动不了   
USER seluser